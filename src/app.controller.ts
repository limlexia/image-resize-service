import { Controller, Get, Res, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { Request, Response } from "express";
import * as fs from "fs";
import path = require("path");
import * as util from "util";
import sharp = require('sharp');

const asyncReadFileSync = util.promisify(fs.readFile);


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get([
    "*.jpg",
    "*.jpeg",
    "*.png",
    "*.webp",
    "*.bmp"
  ])
  async getJpg(@Req() req: Request, @Res() res: Response) {
    const filePath = path.join(__dirname, "/statics", req.path);
    const extName = path.extname(filePath).replace(".", "").toLowerCase();

    if (fs.lstatSync(filePath).isFile()) {
      if (Object.keys(req.query).length > 0) {
        if (!req.query.format) {
          req.query.format = extName;
        }

        const options = this.appService.getOptions(req.query, res);

        try {
          return res.send(await this.appService.resizeImage(filePath, options));
        } catch (e) {
          res.setHeader("Content-Type", this.appService.getContentsType(extName));
          return res.send(await asyncReadFileSync(filePath));
        }
      } else {
        res.setHeader("Content-Type", this.appService.getContentsType(extName));
        return res.send(await asyncReadFileSync(filePath));
      }
    } else {
      return res.sendStatus(404);
    }
  }
}
