import { Injectable } from '@nestjs/common';
import * as sharp from "sharp";
import { Response } from "express";

type OutPutType = "png" | "webp" | "jpeg" | "jpg";
type FitType = "contain" | "cover" | "fill" | "inside" | "outside";

interface ResizeOptions {
  width?: number;
  height?: number;
  quality?: number;
  output?: OutPutType;
  fit?: FitType;
}

const OutputValues = ["png", "webp", "jpeg", "jpg", "bmp"];
const fitValues = ["contain", "cover", "fill", "inside", "outside"];

@Injectable()
export class AppService {
  async resizeImage(filePath: string, options?: ResizeOptions) {
    const img = sharp(filePath).withMetadata().ensureAlpha();

    if (options.width || options.height) {
      img.resize({
        height: options.height,
        width: options.width,
        fit: options.fit,
        background: {
          alpha: 0,
          b:0,
          r:0,
          g:0
        }
      });
    }

    if (options.output === "png") {
      return await img.png({
        palette: true, 
        quality: options.quality
      }).toBuffer();
    } else if (options.output === "webp") {
      return await img.webp({ quality: options.quality}).toBuffer();
    } else {
      return await img.jpeg({ quality: options.quality}).toBuffer();
    }
  }

  getOptions(query: any, res: Response) {
    const options = {} as ResizeOptions;

    if (query.size) {
      const size = (query.size as string).split("x");
      options.width = Number(size[0]);
      options.height = Number(size[1]);
    } else if (query.width) {
      options.width = Number(query.width);
    } else if (query.height) {
      options.height = Number(query.height);
    }

    if (query.format && OutputValues.includes(query.format?.toLowerCase())) {
      options.output = (query.format as string).toLowerCase() as OutPutType;
      res.setHeader("Content-Type", this.getContentsType(options.output));
    }

    if (query.fit && fitValues.includes(query.fit?.toLowerCase())) {
      options.fit = (query.fit as string).toLowerCase() as FitType;
    } else {
      options.fit = "inside";
    }

    options.quality = Number(query.quality) || 90;

    return options;
  }

  getContentsType(format: string) {
    if (format === "webp") {
      return "image/webp";
    } else if (format === "png") {
      return "image/png";
    } else if (format === "jpeg" || format == "jpg") {
      return "image/jpeg";
    } else if (format === "gif") {
      return "image/gif";
    } else if (format === "bmp") {
      return "image/bmp";
    }
  }
}
